package oop.bad

import oop.{AuthorizedUser, UserId, UserMessage}

abstract class AuthorizedUserApi(authorizedUser: AuthorizedUser)
    extends AnyUserApi(authorizedUser) {

  protected def preprocessHttpMethod(method: String): Unit

  override def sendMessage(to: UserId, messageText: String): Unit = {
    val message = UserMessage(authorizedUser, messageText)
    val method  = getUserById(to) match {
      case Some(userTo) => s"send to $userTo message: $message"
      case None         => s"User $to not found"
    }

    preprocessHttpMethod(method)
    println(s"HTTP: $method")
  }

  protected def getUserById(userId: UserId): Option[AuthorizedUser]
}
