package oop.bad

import oop.AuthorizedUser

class LoggingPostgresAuthorizedUserApi(authorizedUser: AuthorizedUser) extends PostgresAuthorizedUserApi(authorizedUser) {
  override protected def preprocessHttpMethod(method: String): Unit = println(s"LOG: $method")
}
