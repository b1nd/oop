package oop.bad

import oop.AuthorizedUser

class NonLoggingPostgresAuthorizedUserApi(authorizedUser: AuthorizedUser)
    extends PostgresAuthorizedUserApi(authorizedUser) {
  override protected def preprocessHttpMethod(method: String): Unit = ()
}
