package oop.bad

import oop._

object App {
  def main(args: Array[String]): Unit = {
    val session = new UserSession {
      override def getUser: User =
        AuthorizedUser(UserId(1), "Alexey", 13, Russia)
    }

    val api: Api = session.getUser match {
      case authorized: AuthorizedUser =>
        if (authorized.region == Russia) {
          if (authorized.age < 18) {
            new LoggingPostgresAuthorizedUserApi(authorized)
          } else new NonLoggingPostgresAuthorizedUserApi(authorized)
        } else ??? // todo: Mongo
      case user: User => new AnyUserApi(user)
    }

   // api.sendMessage(UserId(1), "Hello there")
  }
}
