package oop.bad

import oop.{Api, User, UserId, UserMessage}

class AnyUserApi(user: User) extends Api {
  override def tweet(message: String): Unit =
    println(s"HTTP: $user: $message")

  override def sendMessage(to: UserId, messageText: String): Unit = throw new IllegalStateException("Not allowed")

  override def getMessages: List[UserMessage] = throw new IllegalStateException("Not allowed")
}
