package oop.bad

import oop.{AuthorizedUser, Russia, USA, UserId, UserMessage}

abstract class MongoAuthorizedUserApi(authorizedUser: AuthorizedUser)
    extends AuthorizedUserApi(authorizedUser) {
  override def getMessages: List[UserMessage] = {
    println(
      s"MONGO: GET message from messages where user_id = ${authorizedUser.id}"
    )
    List(
      UserMessage(AuthorizedUser(UserId(123), "Alexey", 19, Russia), "Privet"),
      UserMessage(AuthorizedUser(UserId(124), "Mike", 13, USA), "Hello")
    )
  }

  override def getUserById(userId: UserId): Option[AuthorizedUser] = {
    println(s"MONGO: GET user from users where id = $userId")
    Some(AuthorizedUser(userId, "Mike", 13, USA))
  }
}
