package oop

case class UserMessage(from: AuthorizedUser, text: String)
