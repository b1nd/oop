package oop

trait User {
  def name: String
}

case class UnauthorizedUser(name: String) extends User
case class AuthorizedUser(id: UserId, name: String, age: Int, region: Region) extends User

case class UserId(id: Int)

sealed trait Region
case object Russia extends Region
case object USA extends Region
