package oop.solid.api

trait Tweetable {
  def tweet(message: String): Unit
}
