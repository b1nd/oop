package oop.solid.api

import oop.solid.dao.UserDao
import oop.solid.http.HttpClient
import oop.{Api, AuthorizedUser, UserId, UserMessage}

class AuthorizedUserApi(
  authorizedUser: AuthorizedUser,
  tweetable: Tweetable,
  userDao: UserDao,
  httpClient: HttpClient
) extends Api {
  override def tweet(message: String): Unit = tweetable.tweet(message)

  override def sendMessage(to: UserId, messageText: String): Unit = {
    val message = UserMessage(authorizedUser, messageText)
    val method  = userDao.getByUserId(to) match {
      case Some(userTo) => s"send to $userTo message: $message"
      case None         => s"User $to not found"
    }

    httpClient.call(method)
  }

  override def getMessages: List[UserMessage] =
    userDao.getMessagesByUserId(authorizedUser.id)
}
