package oop.solid.api

import oop.User
import oop.solid.http.HttpClient

class HttpTweetable(user: User, httpClient: HttpClient) extends Tweetable {
  override def tweet(message: String): Unit =
    httpClient.call(s"$user: $message")
}
