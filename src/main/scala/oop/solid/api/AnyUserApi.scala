package oop.solid.api

import oop.{Api, UserId, UserMessage}

class AnyUserApi(tweetable: Tweetable) extends Api {
  override def tweet(message: String): Unit = tweetable.tweet(message)

  override def sendMessage(to: UserId, messageText: String): Unit = throw new IllegalStateException("Not allowed")

  override def getMessages: List[UserMessage] = throw new IllegalStateException("Not allowed")
}
