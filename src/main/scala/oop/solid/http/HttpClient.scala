package oop.solid.http

trait HttpClient {
  def call(method: String): Unit
}
