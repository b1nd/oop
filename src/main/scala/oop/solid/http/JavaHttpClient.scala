package oop.solid.http

class JavaHttpClient extends HttpClient {
  override def call(method: String): Unit = println(s"HTTP: $method")
}
