package oop.solid.http

import oop.solid.logging.Logger

class LoggingHttpClient(httpClient: HttpClient, logger: Logger)
    extends HttpClient {
  override def call(method: String): Unit = {
    logger.log(method)
    httpClient.call(method)
  }
}
