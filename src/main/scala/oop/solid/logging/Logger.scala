package oop.solid.logging

trait Logger {
  def log(string: String): Unit
}
