package oop.solid.logging

class StdOutLogger extends Logger {
  override def log(string: String): Unit = println(s"LOG: $string")
}
