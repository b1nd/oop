package oop.solid.dao
import oop.{AuthorizedUser, Russia, USA, UserId, UserMessage}

class PostgresUserDao extends UserDao {
  override def getByUserId(userId: UserId): Option[AuthorizedUser] = {
    println(s"SQL: select * from users where id = $userId")
    Some(AuthorizedUser(userId, "Mike", 13, USA))
  }

  override def getMessagesByUserId(userId: UserId): List[UserMessage] = {
    println(s"SQL: select * from messages where user_id = $userId")
    List(
      UserMessage(AuthorizedUser(UserId(123), "Alexey", 19, Russia), "Privet"),
      UserMessage(AuthorizedUser(UserId(124), "Mike", 13, USA), "Hello")
    )
  }
}
