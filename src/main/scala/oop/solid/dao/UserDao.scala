package oop.solid.dao

import oop.{AuthorizedUser, UserId, UserMessage}

trait UserDao {
  def getByUserId(userId: UserId): Option[AuthorizedUser]
  def getMessagesByUserId(userId: UserId): List[UserMessage]
}
