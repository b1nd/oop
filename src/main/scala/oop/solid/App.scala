package oop.solid

import oop.solid.api.{AnyUserApi, AuthorizedUserApi, HttpTweetable, Tweetable}
import oop.solid.dao.{MongoUserDao, PostgresUserDao, UserDao}
import oop.solid.http.{HttpClient, JavaHttpClient, LoggingHttpClient}
import oop._
import oop.solid.logging.{Logger, StdOutLogger}

object App {
  def main(args: Array[String]): Unit = {
    val session                = new UserSession {
      override def getUser: User =
        AuthorizedUser(UserId(11), "Alexey", 17, USA)
    }
    val javaClient: HttpClient = new JavaHttpClient
    val tweetable: Tweetable   = new HttpTweetable(session.getUser, javaClient)
    val logger: Logger         = new StdOutLogger()
    val api: Api               = session.getUser match {
      case authorized: AuthorizedUser =>
        val httpClient: HttpClient = if (authorized.age < 18) {
          new LoggingHttpClient(javaClient, logger)
        } else javaClient
        val userDao: UserDao       =
          if (authorized.region == Russia) new PostgresUserDao
          else new MongoUserDao
        new AuthorizedUserApi(authorized, tweetable, userDao, httpClient)
      case _                          => new AnyUserApi(tweetable)
    }

    api.sendMessage(UserId(222), "Hello world")
  }
}
