package oop

trait Api {
  def tweet(message: String): Unit
  def sendMessage(to: UserId, messageText: String): Unit
  def getMessages: List[UserMessage]
}
