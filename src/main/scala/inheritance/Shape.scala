package inheritance

trait Shape {
  def area: Double
}

class Rectangle private (private var x: Double, private var y: Double)
    extends Shape {
  override def area: Double = x * y

  def setX(x: Double): Unit = {
    require(x > 0, "Error: x < 0")
    this.x = x
  }
}

object Rectangle {
  def apply(x: Double, y: Double): Rectangle = {
    require(x > 0, "Error: x < 0")
    require(y > 0, "Error: y < 0")
    new Rectangle(x, y)
  }
}

class Circle(r: Double) extends Shape {
  val Pi                    = 3.14
  override def area: Double = Pi * r * r
}

class ShapeCollection[T <: Shape](val shapes: List[T]) {
  def areaSum: Double             = shapes.map(shape => shape.area).sum
  def rectangles: List[Rectangle] = shapes.collect { case r: Rectangle => r }
}

object Main {
  def main(args: Array[String]): Unit = {
    val rectangle = Rectangle(1, 2)
    val rectangle2 = Rectangle(3, 4)
    val circle    = new Circle(2)

    val collection = new ShapeCollection[Shape](List(rectangle, rectangle2, circle))

    println(collection.areaSum)
    println(collection.rectangles.size)
  }

  def sumShapes[T <: Shape](shapes: List[T]): Double =
    shapes.map(shape => shape.area).sum
}
